# myinterview

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## My interview

- `open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security`

# Reference

- https://www.digitalocean.com/community/tutorials/vuejs-using-bootstrap4
- https://www.digitalocean.com/community/tutorials/vuejs-vue-sweetalert2
